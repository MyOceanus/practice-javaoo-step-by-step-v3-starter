package ooss;

import java.util.List;
import java.util.Objects;

public class Klass {
    private int number;

    private List<Person> attachedPersons;

    public Klass(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    public void assignLeader(Student student){
        if(student != null && student.getKlass() == null){
            System.out.println("It is not one of us.");
        }
        if(student!=null){
        student.setLeader(true);
        attachedPersons.forEach(attachedPerson->attachedPerson.speak(student));
        }
    }

    public Boolean isLeader(Student student){
        if(student != null && student.getKlass()!=null){
            return student.getLeader();
        }
        return false;
    }

    public void attach(Person person){
        this.attachedPersons.add(person);
    }

}
