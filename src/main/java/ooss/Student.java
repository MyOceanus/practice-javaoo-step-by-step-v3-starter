package ooss;

public class Student extends Person {

    private Klass klass;

    private Boolean isLeader = false;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String studentIntroduce = super.introduce()+" I am a student.";
        if(klass!=null){
            if(isLeader){
                studentIntroduce += String.format(" I am the leader of class %d.",klass.getNumber());
            }else {
            studentIntroduce += String.format(" I am in class %d.",klass.getNumber());
            }
        }
        return studentIntroduce;
    }

    public void join(Klass klass){
        this.klass = klass;
    }

    public Boolean isIn(Klass klass){
        if(this.klass == null){
            return false;
        }
        return this.klass.equals(klass);
    }

    public Klass getKlass() {
        return klass;
    }

    public Boolean getLeader() {
        return isLeader;
    }

    public void setLeader(Boolean leader) {
        isLeader = leader;
    }

    @Override
    public void speak(Student student) {
        System.out.println(String.format("I am %s, student of Class %d. I know %s become Leader.",super.getName(),klass.getNumber(),student.getName()));
    }
}
