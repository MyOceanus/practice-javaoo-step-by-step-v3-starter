package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {

    List<Klass> kClasses = new ArrayList<>();
    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String teacherIntroduce = super.introduce() + " I am a teacher.";
        if(kClasses != null){
            String numbers = teacherKlass(kClasses);

            teacherIntroduce += String.format(" I teach Class %s.",numbers);
        }
        return teacherIntroduce;
    }

    public String teacherKlass(List<Klass> kClasses){
       return kClasses.stream()
                .map(klass -> klass.getNumber())
                .map(String::valueOf)
                .collect(Collectors.joining(", "));
    }

    public void assignTo(Klass klass){
        kClasses.add(klass);
    }

    public Boolean belongsTo(Klass klass){
        if(kClasses == null){
            return false;
        }
        return kClasses.contains(klass);
    }

    public Boolean isTeaching(Student student){
        if(kClasses==null || student.getKlass()==null){
            return false;
        }
        return kClasses.contains(student.getKlass());
    }

    @Override
    public void speak(Student student) {
        System.out.println(String.format("I am %s, teacher of Class %s. I know %s become Leader.",super.getName(),teacherKlass(kClasses),student.getName()));
    }
}
